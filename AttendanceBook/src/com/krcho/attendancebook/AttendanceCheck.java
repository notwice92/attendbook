package com.krcho.attendancebook;

import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.krcho.attendancebook.util.AttendanceCheckListAdapter;
import com.krcho.attendancebook.util.AttendanceSQLiteOpenHelper;
import com.krcho.attendancebook.util.GpsInfo;
import com.krcho.attendancebook.vo.AttendRow;
import com.opencsv.CSVWriter;

public class AttendanceCheck extends Activity implements OnClickListener,
		OnItemLongClickListener {

	Button attend, leave, export;
	ListView list;
	AttendanceCheckListAdapter adapter;
	ArrayList<AttendRow> listArray = new ArrayList<AttendRow>();

	SQLiteDatabase db;
	AttendanceSQLiteOpenHelper dbhelper;
	String com_name, com_lat, com_long;

	GpsInfo gps;

	String savedfilepath;
	File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attendance_check);

		Intent prevIntent = getIntent();
		com_name = prevIntent.getStringExtra("name");
		com_lat = prevIntent.getStringExtra("lat");
		com_long = prevIntent.getStringExtra("long");

		setTitle(com_name + "의 출근부");

		dbhelper = new AttendanceSQLiteOpenHelper(getApplicationContext(),
				com_name + ".db", null, 1);

		attend = (Button) findViewById(R.id.check_btn_attend);
		attend.setOnClickListener(this);

		leave = (Button) findViewById(R.id.check_btn_leave);
		leave.setOnClickListener(this);

		export = (Button) findViewById(R.id.check_btn_export);
		export.setOnClickListener(this);

		list = (ListView) findViewById(R.id.check_list);
		adapter = new AttendanceCheckListAdapter(AttendanceCheck.this,
				R.layout.row_attendance_check, listArray);
		list.setAdapter(adapter);
		list.setOnItemLongClickListener(this);

		loadAttendanceCheckData();
	}

	public void loadAttendanceCheckData() {
		listArray.clear();

		AttendRow init = new AttendRow();
		init.setTag("태그");
		init.setCompany("회사명");
		init.setTime("시간");
		listArray.add(init);

		db = dbhelper.getReadableDatabase();
		Cursor c = db.query("attendance", null, null, null, null, null, null);

		while (c.moveToNext()) {
			AttendRow temp = new AttendRow();
			temp.setCompany(c.getString(c.getColumnIndex("company")));
			temp.setTag(c.getString(c.getColumnIndex("tag")));
			temp.setTime(c.getString(c.getColumnIndex("time")));
			temp.setLatitude(c.getString(c.getColumnIndex("latitude")));
			temp.setLongitude(c.getString(c.getColumnIndex("longitude")));
			temp.setMemo(c.getString(c.getColumnIndex("memo")));

			listArray.add(temp);
		}
		
		Comparator<AttendRow> sort = new Comparator<AttendRow>(){

			@Override
			public int compare(AttendRow lhs, AttendRow rhs) {
				// TODO Auto-generated method stub
				DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm");
				Date lhs_t = new Date();
				Date rhs_t = new Date();
				try {
					lhs_t = formatter.parse(lhs.getTime());
					rhs_t = formatter.parse(rhs.getTime());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return rhs_t.compareTo(lhs_t);
			}
			
		};
		
		Collections.sort(listArray, sort);
		adapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.attendance_check, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.check_btn_attend:
			insert("출근");
			break;
		case R.id.check_btn_leave:
			insert("퇴근");
			break;
		case R.id.check_btn_export:
			if (export(com_name)) {
				Toast.makeText(AttendanceCheck.this, "정상적으로 저장되었습니다.",
						Toast.LENGTH_LONG).show();
				AlertDialog.Builder gsDialog = new AlertDialog.Builder(this);
				gsDialog.setTitle("파일공유");
				gsDialog.setMessage("지금 저장한 파일을 공유할수 있습니다.");
				gsDialog.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								Intent intentSend = new Intent(
										Intent.ACTION_SEND);
								intentSend.setType("text/csv");
								intentSend.putExtra(Intent.EXTRA_STREAM,
										Uri.fromFile(file));
								startActivity(Intent.createChooser(intentSend,
										"공유"));
							}
						})
						.setNegativeButton("NO",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {

									}
								}).create().show();

			} else {
				Toast.makeText(AttendanceCheck.this, "저장이 실패하였습니다.",
						Toast.LENGTH_LONG).show();
			}
			break;
		}
	}

	public void insert(String tag) {
		gps = new GpsInfo(AttendanceCheck.this);
		// GPS 사용유무 가져오기
		if (gps.isGetLocation()) {
			final String type = tag;

			AlertDialog.Builder alert = new AlertDialog.Builder(
					AttendanceCheck.this);

			alert.setTitle(tag + "등록");
			alert.setMessage("특이사항을 입력해주세요!");

			// Set an EditText view to get user input
			final EditText input = new EditText(AttendanceCheck.this);
			alert.setView(input);

			alert.setPositiveButton("확인",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							String memo = input.getText().toString();
							// Do something with value!

							SimpleDateFormat sdfNow = new SimpleDateFormat(
									"yyyy/MM/dd HH:mm");
							String time = sdfNow.format(new Date(System
									.currentTimeMillis()));

							db = dbhelper.getWritableDatabase();

							ContentValues values = new ContentValues();

							values.put("company", com_name);
							values.put("tag", type);
							values.put("Latitude", gps.getLatitude() + "");
							values.put("Longitude", gps.getLongitude() + "");
							values.put("memo", memo);
							values.put("time", time);

							db.insert("attendance", null, values);

							loadAttendanceCheckData();
						}
					});

			alert.setNegativeButton("취소",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							dialog.dismiss();
						}
					});

			AlertDialog dialog = alert.create();
			dialog.getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
			dialog.show();

		} else {
			// GPS 를 사용할수 없으므로
			chkGpsService();
		}

	}

	public boolean chkGpsService() {

		String gps = android.provider.Settings.Secure.getString(
				getContentResolver(),
				android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

		Log.d(gps, "aaaa");

		if (!(gps.matches(".*gps.*") && gps.matches(".*network.*"))) {

			// GPS OFF 일때 Dialog 표시
			AlertDialog.Builder gsDialog = new AlertDialog.Builder(this);
			gsDialog.setTitle("위치 서비스 설정");
			gsDialog.setMessage("무선 네트워크 사용, GPS 위성 사용을 모두 체크하셔야 정확한 위치 서비스가 가능합니다.\n위치 서비스 기능을 설정하시겠습니까?");
			gsDialog.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// GPS설정 화면으로 이동
							Intent intent = new Intent(
									android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							intent.addCategory(Intent.CATEGORY_DEFAULT);
							startActivity(intent);
						}
					})
					.setNegativeButton("NO",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									Toast.makeText(getApplicationContext(),
											"서비스를 이용할 수 없습니다.",
											Toast.LENGTH_LONG).show();
								}
							}).create().show();
			return false;

		} else {
			return true;
		}
	}

	public boolean export(String filename) {

		File exportDir = new File(Environment.getExternalStorageDirectory(),
				"모바일출근부");
		if (!exportDir.exists()) {
			exportDir.mkdirs();
		}

		file = new File(exportDir, filename + ".csv");
		try {
			file.createNewFile();
			CSVWriter csvWrite = new CSVWriter(new FileWriter(file));

			// write company data
			String nextLine[] = { "회사명", com_name, "위도" + com_lat, "경도",
					com_long };
			csvWrite.writeNext(nextLine);

			// write attendance data
			db = dbhelper.getReadableDatabase();
			Cursor curCSV = db.rawQuery("SELECT * FROM attendance", null);
			csvWrite.writeNext(curCSV.getColumnNames());
			while (curCSV.moveToNext()) {
				// Which column you want to exprort
				String arrStr[] = { curCSV.getString(0), curCSV.getString(1),
						curCSV.getString(2), curCSV.getString(3),
						curCSV.getString(4), curCSV.getString(5),
						curCSV.getString(6) };
				csvWrite.writeNext(arrStr);
			}
			csvWrite.close();
			curCSV.close();

			savedfilepath = file.getAbsolutePath();
			return true;
		} catch (Exception sqlEx) {
			Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
			return false;
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		// TODO Auto-generated method stub
		final int index = position;
		
		AlertDialog.Builder gsDialog = new AlertDialog.Builder(this);
		gsDialog.setTitle("상세정보");
		gsDialog.setMessage("태그 : " + listArray.get(position).getTag() + "\n"
				+ "회사 : " + listArray.get(position).getCompany() + "\n"
				+ "시간 : " + listArray.get(position).getTime() + "\n" + "위도 : "
				+ listArray.get(position).getLatitude() + "\n" + "경도 : "
				+ listArray.get(position).getLongitude() + "\n" + "메모 : "
				+ listArray.get(position).getMemo() + "\n");
		gsDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// GPS설정 화면으로 이동
				dialog.dismiss();
			}
		});
		gsDialog.setNegativeButton("삭제", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				db = dbhelper.getWritableDatabase();

				ContentValues values = new ContentValues();

				values.put("company", listArray.get(index).getCompany());
				values.put("tag", listArray.get(index).getTag());
				values.put("time", listArray.get(index).getTime());

				db.execSQL("delete from attendance where"
				+" company = '" + listArray.get(index).getCompany() + "'"
				+" and tag = '"+ listArray.get(index).getTag() + "'"
				+" and time = '"+ listArray.get(index).getTime() + "'" 
				+";");

				loadAttendanceCheckData();
			}
		});

		gsDialog.create().show();

		return false;
	}
}
