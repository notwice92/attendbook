package com.krcho.attendancebook.util;

import java.io.File;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class CompanySQLiteOpenHelper extends SQLiteOpenHelper {
	String dbname;
	File dbpath;

	public CompanySQLiteOpenHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
		dbname = name;
		dbpath = context.getDatabasePath(dbname);
		new SQLiteDatabaseUtil().importDB(dbname, dbpath);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String sql = "create table company ("
				+ "_id integer primary key autoincrement, " + "name text, "
				+ "latitude text, " + "longitude text)";

		db.execSQL(sql);
		Log.d("dbutil", "exec");
		new SQLiteDatabaseUtil().importDB(dbname, dbpath);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	public synchronized void close() {
		// TODO Auto-generated method stub
		super.close();

		new SQLiteDatabaseUtil().exportDB(dbname, dbpath);
	}

	@Override
	public SQLiteDatabase getReadableDatabase() {
		// TODO Auto-generated method stub
		new SQLiteDatabaseUtil().exportDB(dbname, dbpath);
		return super.getReadableDatabase();
	}

}
