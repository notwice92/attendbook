package com.krcho.attendancebook.util;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.krcho.attendancebook.R;
import com.krcho.attendancebook.vo.AttendRow;

public class AttendanceCheckListAdapter extends ArrayAdapter<AttendRow> {
	private static final String TAG = "PageAdapter";
	private ArrayList<AttendRow> info = null;
	private Context mContext = null;
	private int mResource = 0;

	public AttendanceCheckListAdapter(Context context, int textViewResourceId,
			ArrayList<AttendRow> objects) {
		super(context, textViewResourceId, objects);

		// Log.i(TAG, "PageAdapter");
		this.mContext = context;
		this.mResource = textViewResourceId;
		this.info = objects;
	}

	public static class ViewHolder {
		TextView tag, company, time;

		public ViewHolder() {
		}

		public ViewHolder(TextView tag, TextView company, TextView time) {
			this.tag = tag;
			this.company = company;
			this.time = time;
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// //Log.i(TAG, "getView");

		ViewHolder holder;
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(mResource, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (info != null && info.size() > 0) {
			AttendRow data = info.get(position);

			holder.tag = (TextView) convertView.findViewById(R.id.tag);
			holder.tag.setText(data.getTag());
			holder.company = (TextView) convertView.findViewById(R.id.company);
			holder.company.setText(data.getCompany());
			holder.time = (TextView) convertView.findViewById(R.id.time);
			holder.time.setText(data.getTime());

		}

		return convertView;
	}
}
