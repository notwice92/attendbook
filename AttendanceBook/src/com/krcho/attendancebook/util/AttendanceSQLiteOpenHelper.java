package com.krcho.attendancebook.util;

import java.io.File;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AttendanceSQLiteOpenHelper extends SQLiteOpenHelper {
	String dbname;
	File dbpath;

	public AttendanceSQLiteOpenHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub

		dbname = name;
		dbpath = context.getDatabasePath(dbname);
		new SQLiteDatabaseUtil().importDB(dbname, dbpath);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String sql = "create table attendance ("
				+ "_id integer primary key autoincrement, " + "company text, "
				+ "time text, " + "tag text, " + "memo text, "
				+ "latitude text, " + "longitude text)";

		db.execSQL(sql);
		Log.d("dbutil", "exec");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	@Override
	public synchronized void close() {
		// TODO Auto-generated method stub
		super.close();

		new SQLiteDatabaseUtil().exportDB(dbname, dbpath);
	}

	@Override
	public SQLiteDatabase getReadableDatabase() {
		// TODO Auto-generated method stub
		new SQLiteDatabaseUtil().exportDB(dbname, dbpath);
		return super.getReadableDatabase();
	}
}
