package com.krcho.attendancebook.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class SQLiteDatabaseUtil {
	String backupDBPath = "/모바일출근부/데이터베이스/";

	public void importDB(String dbname, File dbpath) {
		// TODO Auto-generated method stub

		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();

			File backupdir = new File(sd, backupDBPath);

			if (!backupdir.exists()) {
				backupdir.mkdirs();
			}
			if (!dbpath.exists()) {
				dbpath.mkdirs();
				dbpath.delete();
			}

			backupDBPath += dbname;

			if (sd.canWrite()) {
				File backupDB = dbpath;
				File currentDB = new File(sd, backupDBPath);

				FileChannel src = new FileInputStream(currentDB).getChannel();
				FileChannel dst = new FileOutputStream(backupDB).getChannel();
				dst.transferFrom(src, 0, src.size());
				src.close();
				dst.close();

				Log.d("dbutil", "import");
			}
		} catch (Exception e) {
			Log.d("dbutil", "import fail" + e.getMessage());
		}
	}

	public void exportDB(String dbname, File dbpath) {
		// TODO Auto-generated method stub

		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();

			File backupdir = new File(sd, backupDBPath);

			if (!backupdir.exists()) {
				backupdir.mkdirs();
			}
			if (!dbpath.exists()) {
				dbpath.mkdirs();
				dbpath.delete();
			}

			backupDBPath += dbname;

			if (sd.canWrite()) {
				File currentDB = dbpath;
				File backupDB = new File(sd, backupDBPath);

				FileChannel src = new FileInputStream(currentDB).getChannel();
				FileChannel dst = new FileOutputStream(backupDB).getChannel();
				dst.transferFrom(src, 0, src.size());
				src.close();
				dst.close();
				Log.d("dbutil", "export");
			}
		} catch (Exception e) {
			Log.d("dbutil", "export fail" + e.getMessage());
		}
	}

}
