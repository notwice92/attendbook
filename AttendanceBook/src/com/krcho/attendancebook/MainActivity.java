package com.krcho.attendancebook;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.krcho.attendancebook.util.CompanySQLiteOpenHelper;
import com.krcho.attendancebook.vo.CompanyRow;

public class MainActivity extends Activity {

	Button start;
	Spinner choice;

	SQLiteDatabase db;
	CompanySQLiteOpenHelper dbhelper;

	ArrayList<CompanyRow> companylist = new ArrayList<CompanyRow>();

	boolean selected = false;
	int point = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		dbhelper = new CompanySQLiteOpenHelper(getApplicationContext(),
				"company.db", null, 1);

		start = (Button) findViewById(R.id.main_btn_start);
		start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent;
				if (selected) {
					intent = new Intent(MainActivity.this,
							AttendanceCheck.class);
					intent.putExtra("name", companylist.get(point).getCompanyname());
					intent.putExtra("lat", companylist.get(point).getLatitude());
					intent.putExtra("long", companylist.get(point).getLongitude());

				} else {
					intent = new Intent(MainActivity.this,
							RegisterCompany.class);
				}
				startActivity(intent);
			}
		});

		loadcompany();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		companylist.clear();
		loadcompany();
	}

	public void loadcompany() {
		select();
		choice = (Spinner) findViewById(R.id.main_spinner_company);
		String[] option = new String[companylist.size() + 1];
		option[0] = "ȸ�� ���";
		if (companylist.size() > 0) {
			for (int i = 1; i < option.length; i++) {
				option[i] = companylist.get(i-1).getCompanyname();
			}
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getApplicationContext(), R.layout.row_companyname, option);
		choice.setAdapter(adapter);
		choice.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (position == 0) {
					selected = false;
				} else {
					selected = true;
				}

				point = position-1;
				choice.setPrompt(choice.getAdapter().getItem(position)
						.toString());
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
	}

	public void select() {
		db = dbhelper.getReadableDatabase();
		Cursor c = db.query("company", null, null, null, null, null, null);

		while (c.moveToNext()) {
			CompanyRow temp = new CompanyRow();
			temp.setCompanyname(c.getString(c.getColumnIndex("name")));
			temp.setLatitude(c.getString(c.getColumnIndex("latitude")));
			temp.setLongitude(c.getString(c.getColumnIndex("longitude")));

			companylist.add(temp);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
