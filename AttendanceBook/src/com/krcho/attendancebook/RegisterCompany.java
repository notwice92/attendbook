package com.krcho.attendancebook;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.krcho.attendancebook.util.CompanySQLiteOpenHelper;
import com.krcho.attendancebook.util.GoogleMapkiUtil;

public class RegisterCompany extends FragmentActivity {

	public static LatLng DEFAULT_GP = new LatLng(37.566500, 126.978000);// 서울

	// Minimum & maximum latitude so we can span it
	// The latitude is clamped between -80 degrees and +80 degrees inclusive
	// thus we ensure that we go beyond that number
	private double minLatitude = +81;
	private double maxLatitude = -81;

	// Minimum & maximum longitude so we can span it
	// The longitude is clamped between -180 degrees and +180 degrees inclusive
	// thus we ensure that we go beyond that number
	private double minLongitude = +181;
	private double maxLongitude = -181;

	private ProgressDialog progressDialog;
	private String errorString = "";
	private GoogleMapkiUtil httpUtil;
	private AlertDialog errorDialog;
	private Handler handler;

	EditText keyword;
	Button search;
	GoogleMap map;

	SQLiteDatabase db;
	CompanySQLiteOpenHelper dbhelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_company);

		dbhelper = new CompanySQLiteOpenHelper(getApplicationContext(),
				"company.db", null, 1);

		keyword = (EditText) findViewById(R.id.reg_com_et_keyword);
		search = (Button) findViewById(R.id.reg_com_btn_search);
		search.setClickable(false);
		search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (chkGpsService()) {

					if (keyword.getText().length() > 0) {
						if (progressDialog != null
								&& progressDialog.isShowing())
							return;
						progressDialog = ProgressDialog.show(
								RegisterCompany.this, "Wait", "검색 중입니다");

						Location lo = map.getMyLocation();
						if (lo == null) {
							Toast.makeText(RegisterCompany.this, "위치를 찾는 중입니다. 잠시후에 다시 시도하세요!", Toast.LENGTH_LONG).show();
							progressDialog.dismiss();
						} else {
							String address = getAddres(lo.getLatitude(),
									lo.getLongitude());
							httpUtil.requestMapSearch(new ResultHandler(
									RegisterCompany.this), keyword.getText()
									.toString(), address);

							final InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(
									keyword.getWindowToken(), 0);
						}

					}
				}
			}

		});

		setUpMapIfNeeded();
		httpUtil = new GoogleMapkiUtil();
		errorDialog = new AlertDialog.Builder(this).setTitle("오류")
				.setMessage(errorString).setPositiveButton("확인", null).create();

		handler = new Handler(getMainLooper());
		// 서울로 이동
		goToSeoul();

	}

	private boolean chkGpsService() {

		String gps = android.provider.Settings.Secure.getString(
				getContentResolver(),
				android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

		Log.d(gps, "aaaa");

		if (!(gps.matches(".*gps.*") && gps.matches(".*network.*"))) {

			// GPS OFF 일때 Dialog 표시
			AlertDialog.Builder gsDialog = new AlertDialog.Builder(this);
			gsDialog.setTitle("위치 서비스 설정");
			gsDialog.setMessage("무선 네트워크 사용, GPS 위성 사용을 모두 체크하셔야 정확한 위치 서비스가 가능합니다.\n위치 서비스 기능을 설정하시겠습니까?");
			gsDialog.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// GPS설정 화면으로 이동
							Intent intent = new Intent(
									android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							intent.addCategory(Intent.CATEGORY_DEFAULT);
							startActivity(intent);
						}
					})
					.setNegativeButton("NO",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									Toast.makeText(getApplicationContext(),
											"검색서비스를 이용할 수 없습니다.",
											Toast.LENGTH_LONG).show();
									keyword.setText(null);
								}
							}).create().show();
			return false;

		} else {
			return true;
		}
	}

	private String getAddres(double lat, double lng) {
		Geocoder gcK = new Geocoder(getApplicationContext(), Locale.KOREA);
		String res = "정보없음";
		try {
			List<Address> addresses = gcK.getFromLocation(lat, lng, 1);
			StringBuilder sb = new StringBuilder();

			if (null != addresses && addresses.size() > 0) {
				Address address = addresses.get(0);
				// sb.append(address.getCountryName()).append("/");
				// sb.append(address.getPostalCode()).append("/");
				sb.append(address.getLocality()).append("/");
				sb.append(address.getThoroughfare()).append("/");
				sb.append(address.getFeatureName());
				res = sb.toString();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	static class ResultHandler extends Handler {
		private final WeakReference<RegisterCompany> mActivity;

		ResultHandler(RegisterCompany activity) {
			mActivity = new WeakReference<RegisterCompany>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			RegisterCompany activity = mActivity.get();
			if (activity != null) {
				activity.handleMessage(msg);
			}
		}
	}

	private void handleMessage(Message msg) {
		progressDialog.dismiss();

		String result = msg.getData().getString(GoogleMapkiUtil.RESULT);
		ArrayList<String> searchList = new ArrayList<String>();

		if (result.equals(GoogleMapkiUtil.SUCCESS_RESULT)) {
			searchList = msg.getData().getStringArrayList("searchList");

		} else if (result.equals(GoogleMapkiUtil.TIMEOUT_RESULT)) {
			errorString = "네트워크 연결이 안됩니다.";
			errorDialog.setMessage(errorString);
			errorDialog.show();
			return;
		} else if (result.equals(GoogleMapkiUtil.FAIL_MAP_RESULT)) {
			errorString = "검색이 안됩니다.";
			errorDialog.setMessage(errorString);
			errorDialog.show();
			return;
		} else {
			errorString = httpUtil.stringData;
			errorDialog.setMessage(errorString);
			errorDialog.show();
			return;
		}

		Toast.makeText(this, "Success !!!", Toast.LENGTH_SHORT).show();

		String[] searches = searchList.toArray(new String[searchList.size()]);
		adjustToPoints(searches);
	}

	protected void adjustToPoints(String[] results) {

		map.clear();

		int length = Integer.valueOf(results.length / 3);
		LatLng[] mPoints = new LatLng[length];

		for (int i = 0; i < length; i++) {
			LatLng latlng = new LatLng(Float.valueOf(results[i * 3 + 1]),
					Float.valueOf(results[i * 3 + 2]));
			map.addMarker(new MarkerOptions()
					.position(latlng)
					.title(results[i * 3])
					.icon(BitmapDescriptorFactory.defaultMarker(i * 360
							/ length)));

			mPoints[i] = latlng;
		}

		for (LatLng ll : mPoints) {

			// Sometimes the longitude or latitude gathering
			// did not work so skipping the point
			// doubt anybody would be at 0 0
			if (ll.latitude != 0 && ll.longitude != 0) {
				// Sets the minimum and maximum latitude so we can span and zoom
				minLatitude = (minLatitude > ll.latitude) ? ll.latitude
						: minLatitude;
				maxLatitude = (maxLatitude < ll.latitude) ? ll.latitude
						: maxLatitude;
				// Sets the minimum and maximum latitude so we can span and zoom
				minLongitude = (minLongitude > ll.longitude) ? ll.longitude
						: minLongitude;
				maxLongitude = (maxLongitude < ll.longitude) ? ll.longitude
						: maxLongitude;
			}
		}

		Log.d("dd", minLatitude + "/" + maxLatitude + "/" + minLongitude + "/"
				+ maxLongitude);

		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(new LatLngBounds(
				new LatLng(minLatitude, minLongitude), new LatLng(maxLatitude,
						maxLongitude)), 4);
		map.animateCamera(cu);

	}

	private void goToSeoul() {
		handler.post(findSeoul);
	}

	private Runnable findSeoul = new Runnable() {

		@Override
		public void run() {
			if (map != null) {
				CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(DEFAULT_GP,
						13f);
				map.moveCamera(cu);
				search.setClickable(true);
				Log.d("dd", "OK seoul");
			} else {
				handler.postDelayed(findSeoul, 100);
			}
		}
	};

	protected void onStop() {
		handler.removeCallbacks(findSeoul);
		super.onStop();
	};

	private void setUpMapIfNeeded() {
		if (map == null) {
			map = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			if (map != null) {
				setUpMap();
			}
		}
	}

	private void setUpMap() {
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		map.setMyLocationEnabled(true);

		map.setOnMarkerClickListener(new OnMarkerClickListener() {

			public boolean onMarkerClick(Marker marker) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						RegisterCompany.this);

				alert.setTitle("회사이름 등록");
				alert.setMessage("회사이름을 입력해주세요!");

				// Set an EditText view to get user input
				final EditText input = new EditText(RegisterCompany.this);
				alert.setView(input);

				final LatLng position = marker.getPosition();

				alert.setPositiveButton("확인",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								String value = input.getText().toString();
								// Do something with value!

								insert(value, position);
								AlertDialog.Builder alert = new AlertDialog.Builder(
										RegisterCompany.this);
								alert.setTitle("완료메세지");
								alert.setMessage("위치등록이 완료되었습니다.");
								alert.setPositiveButton("계속등록하기",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												// TODO Auto-generated method
												// stub
												dialog.dismiss();
											}
										});
								alert.setNegativeButton("닫기",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												// TODO Auto-generated method
												// stub
												finish();
											}
										});
								AlertDialog finish_dialog = alert.create();
								finish_dialog.show();
							}
						});

				alert.setNegativeButton("취소",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								dialog.dismiss();
							}
						});

				AlertDialog dialog = alert.create();
				dialog.getWindow()
						.setSoftInputMode(
								WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				dialog.show();

				return false;
			}
		});

		map.setOnMapLongClickListener(new OnMapLongClickListener() {

			@Override
			public void onMapLongClick(LatLng point) {
				// TODO Auto-generated method stub

				AlertDialog.Builder alert = new AlertDialog.Builder(
						RegisterCompany.this);

				alert.setTitle("회사위치 등록");
				alert.setMessage("회사위치를 입력해주세요!");

				// Set an EditText view to get user input
				final EditText input = new EditText(RegisterCompany.this);
				final LatLng clickedpoint = point;
				alert.setView(input);

				alert.setPositiveButton("확인",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								String value = input.getText().toString();
								// Do something with value!
								MarkerOptions markerOptions = new MarkerOptions();
								markerOptions.position(clickedpoint);
								markerOptions.title(value);

								map.animateCamera(CameraUpdateFactory
										.newLatLng(clickedpoint));
								map.addMarker(markerOptions);

								insert(value, markerOptions.getPosition());
								AlertDialog.Builder alert = new AlertDialog.Builder(
										RegisterCompany.this);
								alert.setTitle("완료메세지");
								alert.setMessage("위치등록이 완료되었습니다.");
								alert.setPositiveButton("계속등록하기",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												// TODO Auto-generated method
												// stub
												dialog.dismiss();
											}
										});
								alert.setNegativeButton("닫기",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												// TODO Auto-generated method
												// stub
												finish();
											}
										});
								AlertDialog finish_dialog = alert.create();
								finish_dialog.show();
							}
						});

				alert.setNegativeButton("취소",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								dialog.dismiss();
							}
						});

				AlertDialog dialog = alert.create();
				dialog.getWindow()
						.setSoftInputMode(
								WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				dialog.show();

			}
		});
	}

	public void insert(String name, LatLng position) {
		db = dbhelper.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("name", name);
		values.put("latitude", position.latitude + "");
		values.put("longitude", position.longitude + "");

		db.insert("company", null, values);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register_company, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
