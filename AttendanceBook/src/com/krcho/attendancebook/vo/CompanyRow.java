package com.krcho.attendancebook.vo;

public class CompanyRow {
	String companyname, latitude, longitude;

	public CompanyRow() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CompanyRow(String companyname, String latitude, String longitude) {
		super();
		this.companyname = companyname;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
}
